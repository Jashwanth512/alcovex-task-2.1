import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/loader/loader.service';
import { ReceivedataService } from 'src/app/services/receivedata.service';
@Component({
  selector: 'app-weather-widget-main',
  templateUrl: './weather-widget-main.component.html',
  styleUrls: ['./weather-widget-main.component.css']
})

export class WeatherWidgetMainComponent implements OnInit {
  data:any
  constructor(public loaderService:LoaderService, public receivedService: ReceivedataService) 
  {}
  ngOnInit(): void {
    this.data={
      main:{}
    };
  }  
  
  setData(d:object)
  {
    this.data=d
    this.data.name=this.data.name
    this.data.Temp=this.data.main.temp
    console.log(this.data.name,this.data.Temp)
  }
}