import { Component, OnInit } from '@angular/core';
import { ReceivedataService } from '../../services/receivedata.service';
@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css'],
})
export class SearchbarComponent implements OnInit {
  public city: any;
  constructor(public receiveDataService: ReceivedataService) {}
  ngOnInit(): void {}

  updatingCity(event: any) {
    console.log(event, 'event');
    console.log(this.city, 'city while updating')
  }
  onSubmit() {
    console.log(this.city, 'city')
    this.receiveDataService.receiveData(this.city);
  }
}
