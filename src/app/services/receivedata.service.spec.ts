import { TestBed } from '@angular/core/testing';

import { ReceivedataService } from './receivedata.service';

describe('ReceivedataService', () => {
  let service: ReceivedataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReceivedataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
