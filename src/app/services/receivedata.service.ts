import { Injectable } from '@angular/core';
// import { WeatherWidgetMainComponent } from "../components/weather-widget-main/weather-widget-main.component"
// import {GetdataService} from "./getdata.service"
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class ReceivedataService {
  cityData: any = {
    name: '',
    main: {
      temp: ''
    }
  };
  private url1 = 'https://api.openweathermap.org/data/2.5/weather?q=';
  private url2 = '&appid=d28274b5fe592e1f1e558103a5e66370&units=metric';
  constructor(public http: HttpClient) {}

  public fetchData(city) {
    const finalUrl = this.url1 + city + this.url2;
    return this.http.get(finalUrl);
  }

  public receiveData(requiredCity) {
    this.fetchData(requiredCity).subscribe((data) => {
      console.log(data, 'data in api call');
      this.cityData = data;
      // this.mainComponent.setData(this.cityData)
    }, (err) => {
      console.log(err, 'err caught');
    });
  }
}
